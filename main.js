"use strict"

// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser //
// При виклику функція повинна запитати ім'я та прізвище //

function createNewUser() {
    let firstName = prompt("What is your first name?");
    let lastName = prompt("What is your last name?");

    // При виклику функція createNewUser() повинна запитати дату народження 
    // (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday
    let birthday = prompt("Enter your date of birth (dd.mm.yyyy)", "dd.mm.yyyy");


    let newUser = {
        _username: firstName,
        _surname: lastName,
        getLogin() {
            return this._username[0].toLowerCase() + this._surname.toLowerCase();
        }, 

        // метод getAge() який повертатиме скільки користувачеві років //
        getAge() {
            let year = birthday.slice(-4);
            let month = birthday.slice(3,5);
            let day= birthday.slice(0,2);
            let date = new Date(year, month - 1, day);
            let dateNow = new Date();

        //  Врахуємо високосні роки, створивши змінну leapYear //
            
            let leapYear = Math.floor( (dateNow.getFullYear() - Number(year)) / 4 ); 
            let age = Math.floor ( ( (dateNow - date) / 1000 / 60 / 60 / 24 - leapYear) / 365 );
            return `Your age is ${age} years old!`;
        },

        // метод getPassword(), який повертатиме першу літеру імені користувача у 
        // верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. 
        // (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992)
        getPassword() {
            let year = birthday.slice(-4);
            return this._username[0].toUpperCase() + this._surname.toLowerCase() + year;
        },
    };

    Object.defineProperty(newUser, "username", {
        get(){
            console.log(`The first name is ${this._username}!`);
        },
        set(newValue){
            console.log("You are changing the value of a first name!");
            this._username = newValue;
        },
    });
    Object.defineProperty(newUser, "surname", {
        get(){
            console.log(`The last name is ${this._surname}!`);
        },
        set(newValue){
            console.log("You are changing the value of a last name!");
            this._surname = newValue;
        },
    });
    return newUser;
}

// Викликаємо функцію createNewUser() та записуємо результат(тобто об'єкт newUser)
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


// let a = new Date(2020, 10, 10);
// let b = a.toISOString();
// console.log(a);
// console.log(b);

